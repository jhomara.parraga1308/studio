<?php

  class Evento extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        return $this->db->insert('evento',$datos);
    }

    function obtenerTodos(){
        $listadoEventos=$this->db->get("evento");
        if ($listadoEventos->num_rows()>0) {

          return $listadoEventos->result();
        } else {
          return false;
        }
    }

    public function borrar ($id_eve){

     $this->db->where("id_eve",$id_eve);
     return $this->db->delete("evento");
    }

    //sirve para editar un evento, se debe editar con base a un id, usadno where
    public function obtenerPorId($id_eve){
      $this->db->where("id_eve", $id_eve);
      $evento=$this->db->get("evento");
      if($evento->num_rows()>0){
        return $evento->row();
      }
    }

    function actualizar($id_eve,$datos){
      $this->db->where("id_eve",$id_eve);
      return $this->db->update("evento",$datos);
    }
  } //cierre de la clase

 ?>
