<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("cliente",$datos);
    }
    //funcion para consultar instrcutores
    public function obtenerTodos(){
      $listadoClientes=$this->db->get("cliente");
      if ($listadoClientes->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoClientes->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar instructores
    function borrar($id_cli){
      $this->db->where("id_cli",$id_cli);
      return $this->db->delete("cliente");


    }
     //funcion para consultar un instructor especifico
     function obtenerPorTd($id_cli){
       $this->db->where("id_cli",$id_cli);
       $cliente=$this->db->get("cliente");
       if($cliente->num_rows()>0){
         return$cliente->row();
       }
       return false;
     }
     //funcionn para actualizar un instructor
     function actualizar($id_cli,$datos){
       $this->db->where("id_cli",$id_cli);
       return $this->db->update('cliente',$datos);
     }

  }//Cierre de la clase

 ?>
