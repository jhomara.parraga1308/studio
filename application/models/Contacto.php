<?php

  class Contacto extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        return $this->db->insert('contacto',$datos);
    }

    function obtenerTodos(){
        $listadoContactos=$this->db->get("contacto");
        if ($listadoContactos->num_rows()>0) {

          return $listadoContactos->result();
        } else {
          return false;
        }


    }




    public function borrar ($id_con){

     $this->db->where("id_con",$id_con);
     return $this->db->delete("contacto");



    }

    //funcion para consultar un isntructor
    function obtenerPorId($id_con){
      $this->db->where("id_con",$id_con);
      $contacto=$this->db->get("contacto");
      if ($contacto->num_rows()>0){
        return $contacto->row();
      }
      return false;
}
//funcion para actualizar instructor

    function actualizar($id_con,$datos){
      $this->db->where("id_con",$id_con);
      return $this->db->update('contacto',$datos);
}






  }




 ?>
