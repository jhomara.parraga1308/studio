<?php
  class Fotografo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("fotografo",$datos);
    }
    //funcion para consultar instrcutores
    public function obtenerTodos(){
      $listadoFotografos=$this->db->get("fotografo");
      if ($listadoFotografos->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoFotografos->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar instructores
    function borrar($id_fot){
      $this->db->where("id_fot",$id_fot);
      return $this->db->delete("fotografo");


    }
     //funcion para consultar un instructor especifico
     function obtenerPorTd($id_fot){
       $this->db->where("id_fot",$id_fot);
       $fotografo=$this->db->get("fotografo");
       if($fotografo->num_rows()>0){
         return$fotografo->row();
       }
       return false;
     }
     //funcionn para actualizar un instructor
     function actualizar($id_fot,$datos){
       $this->db->where("id_fot",$id_fot);
       return $this->db->update('fotografo',$datos);
     }

  }//Cierre de la clase

 ?>
