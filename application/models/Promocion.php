<?php

  class Promocion extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        return $this->db->insert('promocion',$datos);
    }

    function obtenerTodos(){
        $listadoPromociones=$this->db->get("promocion");
        if ($listadoPromociones->num_rows()>0) {

          return $listadoPromociones->result();
        } else {
          return false;
        }


    }



    public function borrar ($id_pro){

     $this->db->where("id_pro",$id_pro);
     return $this->db->delete("promocion");



    }

    //funcion para consultar un isntructor
    function obtenerPorId($id_pro){
      $this->db->where("id_pro",$id_pro);
      $promocion=$this->db->get("promocion");
      if ($promocion->num_rows()>0){
        return $promocion->row();
      }
      return false;
}
//funcion para actualizar instructor

    function actualizar($id_pro,$datos){
      $this->db->where("id_pro",$id_pro);
      return $this->db->update('promocion',$datos);
}






  }




 ?>
