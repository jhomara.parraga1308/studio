<?php
  class Editor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("editor_foto",$datos);
    }
    //funcion para consultar instrcutores
    public function obtenerTodos(){
      $listadoEditores=$this->db->get("editor_foto");
      if ($listadoEditores->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoEditores->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar instructores
    function borrar($id_edi){
      $this->db->where("id_edi",$id_edi);
      return $this->db->delete("editor_foto");


    }
     //funcion para consultar un instructor especifico
     function obtenerPorTd($id_edi){
       $this->db->where("id_edi",$id_edi);
       $editor=$this->db->get("editor_foto");
       if($editor->num_rows()>0){
         return$editor->row();
       }
       return false;
     }
     //funcionn para actualizar un instructor
     function actualizar($id_edi,$datos){
       $this->db->where("id_edi",$id_edi);
       return $this->db->update('editor_foto',$datos);
     }

  }//Cierre de la clase

 ?>
