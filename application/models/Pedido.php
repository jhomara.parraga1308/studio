<?php

  class Pedido extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        return $this->db->insert('pedido',$datos);
    }

    function obtenerTodos(){
        $listadoPedidos=$this->db->get("pedido");
        if ($listadoPedidos->num_rows()>0) {

          return $listadoPedidos->result();
        } else {
          return false;
        }
    }

    public function borrar ($id_ped){

     $this->db->where("id_ped",$id_ped);
     return $this->db->delete("pedido");
    }

    //sirve para editar un evento, se debe editar con base a un id, usadno where
    public function obtenerPorTd($id_ped){
      $this->db->where("id_ped", $id_ped);
      $pedido=$this->db->get("pedido");
      if($pedido->num_rows()>0){
        return $pedido->row();
      }
    }

    function actualizar($id_ped,$datos){
      $this->db->where("id_ped",$id_ped);
      return $this->db->update("pedido",$datos);
    }
  } //cierre de la clase

 ?>
