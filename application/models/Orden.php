<?php
  class Orden extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("orden",$datos);
    }
    //funcion para consultar instrcutores
    public function obtenerTodos(){
      $listadoOrdenes=$this->db->get("orden");
      if ($listadoOrdenes->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoOrdenes->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar instructores
    function borrar($id_orden){
      $this->db->where("id_orden",$id_orden);
      return $this->db->delete("orden");


    }
     //funcion para consultar un instructor especifico
     function obtenerPorTd($id_orden){
       $this->db->where("id_orden",$id_orden);
       $orden=$this->db->get("orden");
       if($orden->num_rows()>0){
         return$orden->row();
       }
       return false;
     }
     //funcionn para actualizar un instructor
     function actualizar($id_orden,$datos){
       $this->db->where("id_orden",$id_orden);
       return $this->db->update('orden',$datos);
     }

  }//Cierre de la clase

 ?>
