<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promociones extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Promocion');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('promociones/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['promocion']=$this->Promocion->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('promociones/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoPromocion= array(
          "nombre_pro"=>$this->input->post('nombre_pro'),
          "tipo_pro"=>$this->input->post('tipo_pro'),
          "descuento_pro"=>$this->input->post('descuento_pro'),
          "codigo_pro"=>$this->input->post('codigo_pro'),
      );

      if ($this->Promocion->insertar($datosNuevoPromocion))
      {
        $this->session->set_flashdata("confirmacion","Promociones guardado exitosamente");

      }else {
          $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      } redirect ('promociones/index');

    }

    public function eliminar($id_pro)
    {
      if ($this->Promocion->borrar($id_pro)) {
        $this->session->set_flashdata("confirmacion","Promociones eliminado exitosamente");

      } else {
          $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect ('promociones/index');

    }

    //funcion renderizar vista editar con el instructor
            public function editar ($id_pro){
              $data["promocionEditar"]=
              $this->Promocion->obtenerPorId($id_pro);
              $this->load->view('header');
              $this->load->view('promociones/editar',$data);
              $this->load->view('footer');


            }
    // proceso de actualizacion

            public function procesarActualizacion(){
                $datosEditados= array(
                  "nombre_pro"=>$this->input->post('nombre_pro'),
                  "tipo_pro"=>$this->input->post('tipo_pro'),
                  "descuento_pro"=>$this->input->post('descuento_pro'),
                  "codigo_pro"=>$this->input->post('codigo_pro'),

                 );
              $id_pro=$this->input->post("id_pro");
              if ($this->Promocion->actualizar($id_pro,$datosEditados)) {
                $this->session->set_flashdata("confirmacion","Promoción editado exitosamente");
              }else {
                  $this->session->set_flashdata("error","Error al editar , intente otra vez");
              } redirect ('promociones/index');

            }


}

?>
