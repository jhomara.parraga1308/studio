<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenes extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Orden');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('ordenes/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['ordenes']=$this->Orden->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('ordenes/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoOrden= array(
          "precio_orden"=>$this->input->post('precio_orden'),
          "fecha_entrega_orden"=>$this->input->post('fecha_entrega_orden'),
          "nombre_orden"=>$this->input->post('nombre_orden'),
          "estado_orden"=>$this->input->post('estado_orden'),
      );

      if ($this->Orden->insertar($datosNuevoOrden))
      {
        $this->session->set_flashdata("confirmacion","Orden guardada exitosamente");

      }else {
          $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      } redirect("ordenes/index");

    }

    public function eliminar($id_orden)
    {
      if ($this->Orden->borrar($id_orden)) {
        $this->session->set_flashdata("confirmacion","Orden eliminada exitosamente");
      } else {
          $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect("ordenes/index");

    }
    //function renderizar vista editar con el instructor respectivo
    public function editar($id_orden){
      $data["ordenEditar"]=
       $this->Orden->obtenerPorTd($id_orden);
       $this->load->view('header');
       $this->load->view('ordenes/editar',$data);
       $this->load->view('footer');

    }
    //Proceso de actualizacio
    public function procesarActualizacion(){
      $datosEditados=array(
        "precio_orden"=>$this->input->post('precio_orden'),
        "fecha_entrega_orden"=>$this->input->post('fecha_entrega_orden'),
        "nombre_orden"=>$this->input->post('nombre_orden'),
        "estado_orden"=>$this->input->post('estado_orden')
      );
      $id_orden=$this->input->post("id_orden");
      if($this->Orden->actualizar($id_orden,$datosEditados)){
        $this->session->set_flashdata("confirmacion","Orden editado exitosamente");

    }else{
          $this->session->set_flashdata("error","Error al editar , intente otra vez");

      }redirect("ordenes/index");
    }
}    //cierre de la clase

?>
