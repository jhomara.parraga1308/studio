<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fotografos extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Fotografo');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('fotografos/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['fotografos']=$this->Fotografo->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('fotografos/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoFotografo= array(
          "cedula_fot"=>$this->input->post('cedula_fot'),
          "apellido_fot"=>$this->input->post('apellido_fot'),
          "nombre_fot"=>$this->input->post('nombre_fot'),
          "telefono_fot"=>$this->input->post('telefono_fot'),
          "email_fot"=>$this->input->post('email_fot'),
      );

      if ($this->Fotografo->insertar($datosNuevoFotografo))
      {
        $this->session->set_flashdata("confirmacion","Fotografo guardado exitosamente");
      
      }else {
          $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      } redirect("fotografos/index");

    }

    public function eliminar($id_fot)
    {
      if ($this->Fotografo->borrar($id_fot)) {
        $this->session->set_flashdata("confirmacion","Fotografo eliminado exitosamente");

      } else {
          $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect("fotografos/index");

    }
    //function renderizar vista editar con el instructor respectivo
    public function editar($id_fot){
      $data["fotografoEditar"]=
       $this->Fotografo->obtenerPorTd($id_fot);
       $this->load->view('header');
       $this->load->view('fotografos/editar',$data);
       $this->load->view('footer');

    }
    //Proceso de actualizacio
    public function procesarActualizacion(){
      $datosEditados=array(
        "cedula_fot"=>$this->input->post('cedula_fot'),
        "apellido_fot"=>$this->input->post('apellido_fot'),
        "nombre_fot"=>$this->input->post('nombre_fot'),
        "telefono_fot"=>$this->input->post('telefono_fot'),
        "email_fot"=>$this->input->post('email_fot')
      );
      $id_fot=$this->input->post("id_fot");
      if($this->Fotografo->actualizar($id_fot,$datosEditados)){
        $this->session->set_flashdata("confirmacion","Fotografo editado exitosamente");

    }else{
        $this->session->set_flashdata("error","Error al editar , intente otra vez");

      } redirect("fotografos/index");
    }
}    //cierre de la clase

?>
