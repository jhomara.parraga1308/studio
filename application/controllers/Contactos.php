<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactos extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Contacto');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('contactos/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['contacto']=$this->Contacto->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('contactos/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoContacto= array(
          "nombre_con"=>$this->input->post('nombre_con'),
          "telf_con"=>$this->input->post('telf_con'),
          "correo_con"=>$this->input->post('correo_con'),
          "comentario_con"=>$this->input->post('comentario_con'),
      );

      if ($this->Contacto->insertar($datosNuevoContacto))
      {
        $this->session->set_flashdata("confirmacion","Información guardada exitosamente");
      }else {
        $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      }redirect ('contactos/index');

    }

    public function eliminar($id_con)
    {
      if ($this->Contacto->borrar($id_con)) {
        $this->session->set_flashdata("confirmacion","Información eliminada exitosamente");
      } else {
        $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect('contactos/index');

    }

    //funcion renderizar vista editar con el instructor
            public function editar ($id_con){
              $data["contactoEditar"]=
              $this->Contacto->obtenerPorId($id_con);
              $this->load->view('header');
              $this->load->view('contactos/editar',$data);
              $this->load->view('footer');


            }
    // proceso de actualizacion

            public function procesarActualizacion(){
                $datosEditados= array(
                  "nombre_con"=>$this->input->post('nombre_con'),
                  "telf_con"=>$this->input->post('telf_con'),
                  "correo_con"=>$this->input->post('correo_con'),
                  "comentario_con"=>$this->input->post('comentario_con'),

                 );
              $id_con=$this->input->post("id_con");
              if ($this->Contacto->actualizar($id_con,$datosEditados)) {
                  $this->session->set_flashdata("confirmacion","Información editada exitosamente");
              }else {
                $this->session->set_flashdata("error","Error al editar , intente otra vez");
              } redirect("contactos/index");

            }








}

?>
