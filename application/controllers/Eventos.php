<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Evento');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('eventos/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['evento']=$this->Evento->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('eventos/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoEvento= array(
          "tipo_eve"=>$this->input->post('tipo_eve'),
          "nombre_eve"=>$this->input->post('nombre_eve'),
          "fecha_eve"=>$this->input->post('fecha_eve'),
          "descripcion_eve"=>$this->input->post('descripcion_eve'),
          "direccion_eve"=>$this->input->post('direccion_eve'),
      );

      if ($this->Evento->insertar($datosNuevoEvento))
      {
        $this->session->set_flashdata("confirmacion","Evento guardado exitosamente");

      }else {
        $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      } redirect ('eventos/index');

    }

    public function eliminar($id_eve)
    {
      if ($this->Evento->borrar($id_eve)) {
        $this->session->set_flashdata("confirmacion","Evento eliminado exitosamente");
      } else {
        $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect ('eventos/index');
    }

    //pare renderiza la vista editar con el evento
    public function editar($id_eve){
      $data["eventoEditar"]=$this->Evento->obtenerPorId($id_eve);
      $this->load->view('header');
      $this->load->view('eventos/editar',$data);
      $this->load->view('footer');
    }

    //para que tome los datos uevoss y los guarde, usamos los datos de la funcion guardar porque son ingresos de modificacionnes del listado escogiendo por id.
    public function procesarActualizacion(){
      $datosEditados=array(
        "tipo_eve"=>$this->input->post('tipo_eve'),
        "nombre_eve"=>$this->input->post('nombre_eve'),
        "fecha_eve"=>$this->input->post('fecha_eve'),
        "descripcion_eve"=>$this->input->post('descripcion_eve'),
        "direccion_eve"=>$this->input->post('direccion_eve')
      );
      $id_eve=$this->input->post("id_eve");
      if ($this->Evento->actualizar($id_eve,$datosEditados)) {
        $this->session->set_flashdata("confirmacion","Evento editado exitosamente");
      }else{
        $this->session->set_flashdata("error","Error al editar , intente otra vez");
      } redirect ('eventos/index');

    }

}

?>
