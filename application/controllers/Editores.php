<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editores extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Editor');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('editores/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['editores']=$this->Editor->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('editores/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoEditor= array(
          "cedula_edi"=>$this->input->post('cedula_edi'),
          "apellido_edi"=>$this->input->post('apellido_edi'),
          "nombre_edi"=>$this->input->post('nombre_edi'),
          "telefono_edi"=>$this->input->post('telefono_edi'),
          "email_edi"=>$this->input->post('email_edi'),
      );

      if ($this->Editor->insertar($datosNuevoEditor))
      {
        $this->session->set_flashdata("confirmacion","Editor guardado exitosamente");
      }else {
          $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      }redirect ('editores/index');

    }

    public function eliminar($id_edi)
    {
      if ($this->Editor->borrar($id_edi)) {
        $this->session->set_flashdata("confirmacion","Editor eliminado exitosamente");

      } else {
          $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect('editores/index');

    }
    //function renderizar vista editar con el instructor respectivo
    public function editar($id_edi){
      $data["editorEditar"]=
       $this->Editor->obtenerPorTd($id_edi);
       $this->load->view('header');
       $this->load->view('editores/editar',$data);
       $this->load->view('footer');

    }
    //Proceso de actualizacio
    public function procesarActualizacion(){
      $datosEditados=array(
        "cedula_edi"=>$this->input->post('cedula_edi'),
        "apellido_edi"=>$this->input->post('apellido_edi'),
        "nombre_edi"=>$this->input->post('nombre_edi'),
        "telefono_edi"=>$this->input->post('telefono_edi'),
        "email_edi"=>$this->input->post('email_edi')
      );
      $id_edi=$this->input->post("id_edi");
      if($this->Editor->actualizar($id_edi,$datosEditados)){
        $this->session->set_flashdata("confirmacion","Editor modificado exitosamente");
    }else{
        $this->session->set_flashdata("error","Error al editar , intente otra vez");

      } redirect('editores/index');
    }
}    //cierre de la clase

?>
