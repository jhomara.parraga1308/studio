<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Pedido');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('pedidos/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['pedidos']=$this->Pedido->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('pedidos/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoPedido= array(
          "nombre_ped"=>$this->input->post('nombre_ped'),
          "precio_ped"=>$this->input->post('precio_ped'),
          "fecha_ped"=>$this->input->post('fecha_ped'),
      );

      if ($this->Pedido->insertar($datosNuevoPedido))
      {
        $this->session->set_flashdata("confirmacion","Pedido guardado exitosamente");

      }else {
        $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      }   redirect ('pedidos/index');

    }

    public function eliminar($id_ped)
    {
      if ($this->Pedido->borrar($id_ped)) {
          $this->session->set_flashdata("confirmacion","Pedido eliminado exitosamente");
      } else {
        $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      } redirect ('pedidos/index');

    }
    //function renderizar vista editar con el instructor respectivo
    public function editar($id_ped){
      $data["pedidoEditar"]=
       $this->Pedido->obtenerPorTd($id_ped);
       $this->load->view('header');
       $this->load->view('pedidos/editar',$data);
       $this->load->view('footer');

    }
    //Proceso de actualizacio
    public function procesarActualizacion(){
      $datosEditados=array(
        "nombre_ped"=>$this->input->post('nombre_ped'),
        "precio_ped"=>$this->input->post('precio_ped'),
        "fecha_ped"=>$this->input->post('fecha_ped'),
      );
      $id_ped=$this->input->post("id_ped");
      if($this->Pedido->actualizar($id_ped,$datosEditados)){
          $this->session->set_flashdata("confirmacion","Pedido editado exitosamente");
    }else{
        $this->session->set_flashdata("error","Error al editar , intente otra vez");

      } redirect ('pedidos/index');
    }
}    //cierre de la clase

?>
