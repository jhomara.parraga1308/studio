<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Cliente');
   }
  	public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('clientes/nuevo');
  		$this->load->view('footer');
  	}

    public function index()
    {
        $data['clientes']=$this->Cliente->obtenerTodos();
        $this->load->view('header');
		    $this->load->view('clientes/index',$data);
		    $this->load->view('footer');
    }


    public function guardar(){
        $datosNuevoCliente= array(
          "nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),
          "telf_cli"=>$this->input->post('telf_cli'),
          "fecha_cli"=>$this->input->post('fecha_cli'),
          "correo_cli"=>$this->input->post('correo_cli'),
      );

      if ($this->Cliente->insertar($datosNuevoCliente))
      {
        $this->session->set_flashdata("confirmacion","Cliente guardado exitosamente");

      }else {
        $this->session->set_flashdata("error","Error al guardar , intente otra vez");
      }
        redirect ('clientes/index');
    }

    public function eliminar($id_cli)
    {
      if ($this->Cliente->borrar($id_cli)) {
        $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente");

      } else {
        $this->session->set_flashdata("error","Error al eliminar , intente otra vez");
      }
        redirect('clientes/index');
    }
    //function renderizar vista editar con el instructor respectivo
    public function editar($id_cli){
      $data["clienteEditar"]=
       $this->Cliente->obtenerPorTd($id_cli);
       $this->load->view('header');
       $this->load->view('clientes/editar',$data);
       $this->load->view('footer');

    }
    //Proceso de actualizacio
    public function procesarActualizacion(){
      $datosEditados=array(
        "nombre_cli"=>$this->input->post('nombre_cli'),
        "apellido_cli"=>$this->input->post('apellido_cli'),
        "telf_cli"=>$this->input->post('telf_cli'),
        "fecha_cli"=>$this->input->post('fecha_cli'),
        "correo_cli"=>$this->input->post('correo_cli'),
      );
      $id_cli=$this->input->post("id_cli");
      if($this->Cliente->actualizar($id_cli,$datosEditados)){
        $this->session->set_flashdata("confirmacion","Cliente editado exitosamente");
    }else{
      $this->session->set_flashdata("error","Error al editar , intente otra vez");

      } redirect("clientes/index");
    }
}    //cierre de la clase

?>
