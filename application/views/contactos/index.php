<h1 class="text-center"><b>listado de Contactos</b></h1>
<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Lista De contactos</h1>
                    <br>
                    <br>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/contactos/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Contactos</a>

                  </div>

                  </div>

                  <?php if ($contacto): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_contactos">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Telefono</th>
                          <th>Comentario</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($contacto as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_con ?></td>
                             <td> <?php echo $filaTemporal->nombre_con ?></td>
                             <td> <?php echo $filaTemporal->telf_con?></td>
                             <td> <?php echo $filaTemporal->correo_con ?></td>
                             <td> <?php echo $filaTemporal->comentario_con?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/contactos/editar/<?php echo $filaTemporal->id_con; ?>" title="Editar Promocion" style="color:blue;" >
                                 <i class="glyphicon glyphicon-pencil">Editar</i>
                                 <?php endif; ?>
                               </a>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/contactos/eliminar/<?php echo $filaTemporal->id_con; ?>" title="Borrar fotografo" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_contactos").DataTable();
                  </script>

      </div>
  </div>

</div>
