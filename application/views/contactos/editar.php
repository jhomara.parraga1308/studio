<h1>Editar Contactos</h1>

<form class="" id="frm_editar_contactos" action="<?php echo site_url('contactos/procesarActualizacion'); ?>" method="post">

  <div class="row">

    <div class="col-md-4">
        <label for="">Id:</label>
        <br>
        <input type="number" placeholder="Id" class="form-control" name="id_con" value="<?php echo $contactoEditar->id_con ?>">
    </div>



    <div class="col-md-4">
        <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Escriba su nombre" class="form-control" name="nombre_con" value="<?php echo $contactoEditar->nombre_con ?>" id="nombre_con">
    </div>
    <div class="col-md-4">
        <label for="">Telefono:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="number" placeholder="Ingrese el Telefono" class="form-control" name="telf_con" value="<?php echo $contactoEditar->telf_con ?>" id="telf_con">
    </div>
    <div class="col-md-4">
      <label for="">Correo:</label>
      <br>
      <input type="text" placeholder="Ingrese el Correo" class="form-control" name="correo_con" value="<?php echo $contactoEditar->correo_con ?>" id="correo_con">
    </div>
    <div class="col-md-4">
      <label for="">Comentario:<span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese un Comentario" class="form-control" name="comentario_con" value="<?php echo $contactoEditar->comentario_con ?>" id="comentario_con">
    </div>
  </div>
  <br>

  <br>
  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Editar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/contactos/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_editar_contactos").validate({
    rules:{
      nombre_con:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      telf_con:{
        required: true,
        minlength:3,
        maxlength:250,
      },
      correo_con:{
        required: true,
        minlength:3,
        maxlength:100,
      },
      comentario_con:{
        required: true,
        minlength:3,
        maxlength:100,
      },
    },

    messages:{

    nombre_con:{
      required: "ingrese el nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",
    },
    telf_con:{
      required: "ingrese el descuento",
      minlength:"Descuento incorrecto ingrese 3 digitos",
      maxlength:"Descuento incorrecto ingrese 100 digitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
    correo_con:{
      required: "ingrese el tipo",
      minlength:"El tipo debe tener 3 caracteres",
      maxlength:"Tipo incorrecto",
    },
    comentario_con:{
      required: "ingrese su codigo",
      minlength:"El codigo debe tener 3 caracteres",
      maxlength:"Codigo incorrecto",
    },


}
});


</script>
