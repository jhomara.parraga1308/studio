<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Listado de Fotógrafos</h1>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/fotografos/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Fotografo</a>

                  </div>

                  </div>

                  <?php if ($fotografos): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_fotografos">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>CEDULA</th>
                          <th>APELLIDO</th>
                          <th>NOMBRE </th>
                          <th>TELEFONO</th>
                          <th>EMAIL</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($fotografos as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_fot ?></td>
                             <td> <?php echo $filaTemporal->cedula_fot ?></td>
                             <td> <?php echo $filaTemporal->apellido_fot ?></td>
                             <td> <?php echo $filaTemporal->nombre_fot?></td>
                             <td> <?php echo $filaTemporal->telefono_fot?></td>
                             <td> <?php echo $filaTemporal->email_fot ?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="" title="Editar fotografo" >
                                 <a href="<?php echo site_url(); ?>/fotografos/editar/<?php echo $filaTemporal->id_fot; ?>" title="Editar Fotografo" style="color:blue;">
                                 <i class="mdi mdi-pencil"></i>
                                 Editar
                                 </a>
                                 <i class="glyphicon glyphicon-pencil"></i>
                                 <?php endif; ?>
                               </a>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/fotografos/eliminar/<?php echo $filaTemporal->id_fot; ?>" title="Borrar fotografo" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                <?php else: ?>
                <h1>No hay datos</h1>
                <?php endif; ?>
                <script type="text/javascript">
                $("#tbl_fotografos").DataTable();
                </script>

      </div>
  </div>

</div>
