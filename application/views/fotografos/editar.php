<h1 class="text-center"><b>Editar Fotografo</b></h1>
<form class=""
id="frm_editar_fotografo"
action="<?php echo site_url('fotografos/procesarActualizacion'); ?>" method="post">
    <div class="row">
      <input type="text" name="id_fot" id=id_fot value="<?php echo $fotografoEditar->id_fot; ?>">
      <div class="col-md-4">
          <label for="">Cédula:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number" placeholder="Ingrese la cédula" class="form-control" required min="99999999" name="cedula_fot" value="<?php echo $fotografoEditar->cedula_fot; ?>" id="cedula_fot">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido"
          class="form-control"
          required

          name="apellido_fot" value="<?php echo $fotografoEditar->apellido_fot; ?>"
          id="apellido_fot">
      </div>
      <div class="col-md-4">
        <label for="">NOMBRE:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre"
        class="form-control"
        name="nombre_fot" value="<?php echo $fotografoEditar->nombre_fot; ?>"
        id="nombre_fot">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">TELEFONO:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          required

          name="telefono_fot" value="<?php echo $fotografoEditar->telefono_fot; ?>"
          id="telefono_fot">
      </div>
      <div class="col-md-4">
          <label for="">EMAIL:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo"
          class="form-control"
          required

          name="email_fot" value="<?php echo $fotografoEditar->email_fot; ?>"
          id="email_fot">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/fotografos/procesarActualizacion"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_editar_fotografo").validate({
    rules:{
          cedula_fot:{
            required:true,
            minlength:10,
            maxlength:10,
            digits:true
          },
          apellido_fot:{
            required:true,
            minlength:3,
            maxlength:250,
            letras:true
          },
          nombre_fot:{
            required:true,
            minlength:3,
            maxlength:250

          },
          telefono_fot:{
            required:true,
            minlength:3,
            maxlength:250,
            digits:true

          },
          email_fot:{
            required:true,
            minlength:3,
            maxlength:250

          }
    },
    messages:{
      cedula_fot:{
        required:"Cedula icorrecta ingrese 10 digitos",
        minlength:"Cedula incorrecta, ingrese 10 digitosa",
        maxlength:"Cedula incorrecta, ingrese 10 digitosa",
        digits:"Este campo Solo acepta numeros"

      },
      apellido_fot:{
        required:"Por favor ingrese el primer apellido",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"El apellido debe tener al menos 3 caracteres"
      },
      nombre_fot:{
        required:"Este campo solo acepta letras",
        minlength:"El nombre debe tener al menos 3 letras",
        maxlength:"El nombre debe tener al menos 3 letras",

      },
      telefono_fot:{
        required:"Ingrese el numero de telefono",
        minlength:"Numero de telefono incorrecto, ingrese 10 digitosa",
        maxlength:"Telefono incorrectoa, ingrese 10 digitosa",

      },
      email_fot:{
        required:"Por favor ingrese el email",
        minlength:"Direccion incorrecta, ingrese 10 digitosa",
        maxlength:"Dirección incorrecta, ingrese 10 digitosa",


      }
    }
  }
);
</script>
