<h1 class="text-center"><b>Editar Evento</b></h1>
<br>
<br>
<form class="" id="frm_editar_evento" action="<?php echo site_url(); ?>/eventos/procesarActualizacion" method="post">
    <div class="row text-right">
        <input type="text" name="id_eve" id="id_eve" value="<?php echo $eventoEditar->id_eve; ?>">
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Tipo de evento:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Escriba el tipo de evento" class="form-control" required name="tipo_eve" value="<?php echo $eventoEditar->tipo_eve ?>" id="tipo_eve">
      </div>
      <div class="col-md-4">
          <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el nombre para el evento" class="form-control" required name="nombre_eve" value="<?php echo $eventoEditar->nombre_eve; ?>" id="nombre_eve">
      </div>
      <div class="col-md-4">
        <label for="">Fecha:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" min="2023-06-01" max="2023-12-31" placeholder="Ingrese el Nombre del editor" class="form-control" required name="fecha_eve" value="<?php echo $eventoEditar->fecha_eve; ?>" id="fecha_eve">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Descripción del Evento:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese la descripcion del evento " class="form-control" required name="descripcion_eve" value="<?php echo $eventoEditar->descripcion_eve; ?>" id="descripcion_eve">
      </div>
      <div class="col-md-4">
        <label for="">Dirección del Evento:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Ingrese la direecion del evento" class="form-control" required name="direccion_eve" value="<?php echo $eventoEditar->direccion_eve; ?>" id="direccion_eve">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/eventos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
  $("#frm_editar_evento").validate({
    rules:{
      tipo_eve:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      nombre_eve:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      fecha_eve:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:false
      },
      descripcion_eve:{
        required:true,
        minlength:3,
        maxlength:500,
        letras:true
      },
      direccion_eve:{
        required:true,
        minlength:3,
        maxlength:300,
        letras:true
      }
    },
    messages:{
      tipo_eve:{
        required:"Por favor ingrese el tipo de evento",
        minlength:"Ingrese un tipo de evento válido",
        maxlength:"Ingreso incorrecto"
      },
      nombre_eve:{
        required:"Por favor ingrese un nombre del evento",
        minlength:"El nombre del evento debe tener al menos 3 caracteres",
        maxlength:"Nombre del evento muy largo"

      },
      fecha_eve:{
        required:"Por favor ingrese ela fecha del evento",
        minlength:"La fecha debe tener el siguiente formato DD/MM/AA",
        maxlength:"Fecha muy extensa",
      },
      descripcion_eve:{
        required:"Por favor ingrese una breve descripción del evento",
        minlength:"La descripción debe ser mayor a 3 caracteres",
        maxlength:"Descripción muy larga"
      },
      direccion_eve:{
        required:"Por favor ingrese la dirección",
        minlength:"La dirección debe tener al menos 3 caracteres",
        maxlength:"Dirección Incorrecta"
        }
    }
  });
</script>
