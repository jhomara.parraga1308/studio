<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Listado de Eventos</h1>
                    <br>
                    <br>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/eventos/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Evento</a>

                  </div>

                  </div>

                  <?php if ($evento): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_eventos">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>Tipo</th>
                          <th>Nombre</th>
                          <th>Fecha</th>
                          <th>Descripcion</th>
                          <th>Direccion</th>
                          <th>Editar</th>
                          <th>Borrar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($evento as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_eve ?></td>
                             <td> <?php echo $filaTemporal->tipo_eve ?></td>
                             <td> <?php echo $filaTemporal->nombre_eve ?></td>
                             <td> <?php echo $filaTemporal->fecha_eve?></td>
                             <td> <?php echo $filaTemporal->descripcion_eve?></td>
                             <td> <?php echo $filaTemporal->direccion_eve?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url('/eventos/editar/').$filaTemporal->id_eve;?>" title="Editar Evento" style="color:blue;">
                                 <i class="glyphicon glyphicon-pencil">Editar</i>
                                 <?php endif; ?>
                               </a>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/eventos/eliminar/<?php echo $filaTemporal->id_eve; ?>" title="Borrar evento" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash"> Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_eventos").DataTable();
                  </script>

      </div>
  </div>

</div>
