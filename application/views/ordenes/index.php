<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Lista de Ordenes</h1>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/ordenes/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar nueva orden</a>

                  </div>

                  </div>

                  <?php if ($ordenes): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_ordenes">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>NOMBRE </th>
                          <th>PRECIO</th>
                          <th>FECHA</th>
                          <th>ESTADO</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($ordenes as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_orden ?></td>
                             <td> <?php echo $filaTemporal->nombre_orden?></td>
                             <td> <?php echo $filaTemporal->precio_orden?></td>
                             <td> <?php echo $filaTemporal->fecha_entrega_orden?></td>
                             <td> <?php echo $filaTemporal->estado_orden?></td>
                               <td class="text-center">
                                 <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                                 <a href="" title="Editar Orden" >
                                   <a href="<?php echo site_url(); ?>/ordenes/editar/<?php echo $filaTemporal->id_orden; ?>" title="Editar Orden" style="color:blue;">
                                   <i class="mdi mdi-pencil"></i>
                                   Editar
                                   </a>
                                   <i class="glyphicon glyphicon-pencil"></i>
                                 </a>
                                    <?php endif; ?>
                                &nbsp;&nbsp;
                               </td>
                               <td class="text-center" >
                                 <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                                 <a href="<?php echo site_url(); ?>/ordenes/eliminar/<?php echo $filaTemporal->id_orden; ?>" title="Borrar Orden" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                 <i class="glyphicon glyphicon-trash">Eliminar</i>
                                 </a>
                                 <?php endif; ?>
                               </td>

                           </tr>

                         <?php endforeach; ?>
                       </tbody>
                     </table>
                    <?php else: ?>
                    <h1>No hay datos</h1>
                    <?php endif; ?>

                    <script type="text/javascript">
                    $("#tbl_ordenes").DataTable();
                    </script>


        </div>
    </div>

  </div>
