<h1 class="text-center"><b>Nuevo orden de  trabajo </b></h1>
<form class="" id="frm_nuevo_orden" action="<?php echo site_url(); ?>/ordenes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el nombre de la orden" class="form-control" name="nombre_orden" value="" id="nombre_orden">
      </div>
      <div class="col-md-4">
          <label for="">Precio:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="number"  placeholder="Ingrese el precio" class="form-control" name="precio_orden" value="" id="precio_orden">
      </div>
      <div class="col-md-4">
        <label for="">Fecha:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="date" min="2023-06-01" max="2023-12-31" placeholder="Ingrese el Nombre" class="form-control" name="fecha_entrega_orden" value="" id="fecha_entrega_orden">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Estado:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el estado de la orden" class="form-control" name="estado_orden" value="" id="estado_orden">
      </div>

    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/ordenes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
  $("#frm_nuevo_orden").validate({
    rules:{
      nombre_orden:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      precio_orden:{
        required:true,
        minlength:2,
        maxlength:250,
        min: 0,
        max: 1000
      },
      fecha_entrega_orden:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:false
      },
      estado_orden:{
        required:true,
        minlength:3,
        maxlength:500,
        letras:true
      }
    },
    messages:{
      nombre_orden:{
        required:"Por favor ingrese el nombre de la orden",
        minlength:"Ingrese un nombre valido",
        maxlength:"Ingreso incorrecto"
      },
      precio_orden:{
        required:"Por favor ingrese el precio de la orden",
        minlength:"El precio esta exedido",
        maxlength:"Nombre del evento muy largo",
        min: "Ingrese un precio mayor a 0",
        max: "Ingrese un precio valido"
      },
      fecha_entrega_orden:{
        required:"Por favor ingrese la fecha de la orden",
        minlength:"El nombre debe tener caracteres",
        maxlength:"Fecha muy extensa"
      },
      estado_orden:{
        required:"Por favor ingrese el estado de la orden",
        minlength:"El estado debe tener caracteres",
        maxlength:"Estado muy extensa"
        }
    }
  });
</script>
