<?php if (!$this->session->userdata("conectado")): ?>
  <script type="text/javascript">
  window.location.href="<?php echo site_url('welcome/login'); ?>";

  </script>
<?php endif; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Estudio Fotografico</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/fontAwesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/hero-slider.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/owl-carousel.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/datepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plantilla/css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="<?php echo base_url(); ?>plantilla/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!-- IMPORTACION JQuery v 3.x LIBRERIA PROGRAMAR EN JS para programar con elegancia-->
    		<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
        <!-- importacion de jquery validate es una libreria de JS paara validaciones de alto nivel en formularios-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <!-- Aqui vamos a poner un codigo para validar que solo ingrese letras y caracteress del espanol -->
        <script type="text/javascript">
            jQuery.validator.addMethod("letras", function(value, element) {
            //return this.optional(element) || /^[a-z]+$/i.test(value);
              return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú 1234567890.,""''/@$;:<>-_]*$/.test(value);

            }, "Este campo solo acepta letras");
        </script>
        <!-- IMPORTACION DE FLASHDATAS TOASTR -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!-- Este es el eesttilo css de toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CDN de datatables para hacer tablas y un buscador, tag :link y pegammos el is.css -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <!-- Aqui importamos el min.js -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

<!--
	Venue Template
	http://www.templatemo.com/tm-522-venue
-->
    </head>

<body>

    <div class="wrap">
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <button id="primary-nav-button" type="button">Menu</button>
                        <a href="index.html"><div class="logo">
                            <img src="<?php echo base_url(); ?>plantilla/img/img1.png" alt="Venue Logo">
                        </div></a>
                        <nav id="primary-nav" class="dropdown cf">
                            <ul class="dropdown menu">
                                <li class='active'><a href="<?php echo site_url(); ?>">Inicio</a></li>
                                <li><a href="#">Contenido</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Fotógrafos</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/fotografos/nuevo">Nuevo Fotógrafo</a></li>
                                                <li><a href="<?php echo site_url(); ?>/fotografos/index">Listado de Fotógrafo</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Pedidos</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/pedidos/nuevo">Nuevo Pedido</a></li>
                                                <li><a href="<?php echo site_url(); ?>/pedidos/index">Listado de Pedidos</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Ordenes</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/ordenes/nuevo">Nueva Orden</a></li>
                                                <li><a href="<?php echo site_url(); ?>/ordenes/index">Listado de Ordenes</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Editores</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/editores/nuevo">Nuevo Editor</a></li>
                                                <li><a href="<?php echo site_url(); ?>/editores/index">Listado de Editores</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#">Eventos Fotos</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Eventos</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/eventos/nuevo">Nuevo Evento</a></li>
                                                <li><a href="<?php echo site_url(); ?>/eventos/index">Listado de Eventos</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Promociones</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/promociones/nuevo">Nuevas Promociones</a></li>
                                                <li><a href="<?php echo site_url(); ?>/promociones/index">Listado de Promociones</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li><a href="#">Clientes</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Clientes</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo site_url(); ?>/clientes/nuevo">Nuevo Cliente</a></li>
                                                <li><a href="<?php echo site_url(); ?>/clientes/index">Listado de Clientes</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo site_url(); ?>/contactos/index">Contactos</a></li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
                                      <div class="navbar-profile">
                                        <p class="mb-0 d-none d-sm-block navbar-profile-name"><?php echo $this->session->userdata("conectado")->correo_usu; ?></p>
                                        <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                                      </div>
                                        </a>
                                          <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                                            <div class="dropdown-divider"></div>
                                              <a class="dropdown-item preview-item">
                                                <div class="preview-item-content">
                                                  <p class="preview-subject mb-1" onclick="cerrarSistema();">Salir</p>
                                                    <script type="text/javascript">
                                                      function cerrarSistema(){
                                                          window.location.href="<?php echo site_url('welcome/cerrarSesion'); ?>";
                                                          }

                                                    </script>
                                                </div>
                                              </a>
                                            </div>
                                    </li>

                            </ul>
                        </nav><!-- / #primary-nav -->
                    </div>
                </div>
            </div>
        </header>
    </div>
