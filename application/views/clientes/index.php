<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Lista de clientes</h1>
<br>
                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Cliente</a>

                  </div>

                  </div>

                  <?php if ($clientes): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_clientes">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>NOMBRE</th>
                          <th>APELLIDO</th>
                          <th>TELEFONO </th>
                          <th>FECHA</th>
                          <th>EMAIL</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($clientes as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_cli ?></td>
                             <td> <?php echo $filaTemporal->nombre_cli?></td>
                             <td> <?php echo $filaTemporal->apellido_cli ?></td>
                             <td> <?php echo $filaTemporal->telf_cli ?></td>
                             <td> <?php echo $filaTemporal->fecha_cli?></td>
                             <td> <?php echo $filaTemporal->correo_cli ?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="" title="Editar cliente" >
                                 <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" title="Editar Cliente" style="color:blue;">
                                 <i class="mdi mdi-pencil"></i>
                                 Editar
                                 </a>
                                 <i class="glyphicon glyphicon-pencil"></i>
                                 <?php endif; ?>
                               </a>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>" title="Borrar Clientes" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_clientes").DataTable();
                  </script>

      </div>
  </div>

</div>
