<h1 class="text-center"><b>Nuevo Clientes</b></h1>
<form class="" id="frm_nuevo_cliente" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el nombre" class="form-control" name="nombre_cli" value="" id="nombre_cli">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el apellido" class="form-control" name="apellido_cli" value="" id="apellido_cli">
      </div>
      <div class="col-md-4">
        <label for="">Telefono:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="number" placeholder="Ingrese su telefono" class="form-control" name="telf_cli" value="" id="telf_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Fecha de Nacimiento:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="date" placeholder="Ingrese su fecha de nacimiento" class="form-control" name="fecha_cli" value="" id="fecha_cli">
      </div>
      <div class="col-md-4">
        <label for="">Correo:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="float" placeholder="Ingrese su correo" class="form-control" name="correo_cli" value="" id="correo_cli">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_nuevo_cliente").validate({
    rules:{
      nombre_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:false
      },
      apellido_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      telefono_cli:{
        required:true,
        minlength:3,
        maxlength:500,
        letras:true
      },

      correo_cli:{
        required:true,
        minlength:3,
        maxlength:300,
        letras:true
      }
    },
    messages:{
      nombre_cli:{
        required:"Por favor ingrese el nombre del cliente",
        minlength:"El nombre debe tener 10 carcateres",
        maxlength:"nombre muy extensa"
      },
      apellido_cli:{
        required:"Por favor ingrese el apellido del cliente",
        minlength:"El apellido del cliente debe tener al menos 5 caracteres",
        maxlength:"Nombre del evento muy largo"
      },
      telefono_cli:{
        required:"Por favor ingrese el telefono",
        minlength:"El telefono debe tener 10 umeros",
        maxlength:"TELEFONO incorrecto"
      },
    
      },
      email_cli:{
        required:"Por favor ingrese el correo",
        minlength:"La correo debe tener al menos 10 o mas caracteres",
        maxlength:"Dirección de correo Incorrecta"
        }
    }
  });
</script>
