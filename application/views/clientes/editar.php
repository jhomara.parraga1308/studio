<h1 class="text-center"><b>Editar Cliente</b></h1>
<form class=""
id="frm_editar_cliente"
action="<?php echo site_url('clientes/procesarActualizacion'); ?>" method="post">
    <div class="row">
      <input type="text" name="id_cli" id=id_cli value="<?php echo $clienteEditar->id_cli; ?>">
      <div class="col-md-4">
        <label for="">NOMBRE:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre"
        class="form-control"
        name="nombre_cli" value="<?php echo $clienteEditar->nombre_cli; ?>"
        id="nombre_cli">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido"
          class="form-control"
          required

          name="apellido_cli" value="<?php echo $clienteEditar->apellido_cli; ?>"
          id="apellido_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">TELEFONO:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          required

          name="telf_cli" value="<?php echo $clienteEditar->telf_cli; ?>"
          id="telf_cli">
      </div>
      <div class="col-md-4">
          <label for="">Correo:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo"
          class="form-control"
          required

          name="correo_cli" value="<?php echo $clienteEditar->correo_cli; ?>"
          id="correo_cli">
      </div>
    </div>
    <div class="col-md-4">
        <label for="">Fecha de Nacimiento:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="date" placeholder="Ingrese su fecha de nacimiento" class="form-control" name="fecha_cli" value="" id="fecha_cli">
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/procesarActualizacion"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_editar_cliente").validate({
    rules:{
          nombre_cli:{
            required:true,
            minlength:3,
            maxlength:250

          },
          apellido_cli:{
            required:true,
            minlength:3,
            maxlength:250,
            letras:true
          },
          telefono_cli:{
            required:true,
            minlength:3,
            maxlength:250,
            digits:true

          },
          correo_cli:{
            required:true,
            minlength:3,
            maxlength:250

          }
    },
    messages:{
      nombre_cli:{
        required:"Este campo solo acepta letras",
        minlength:"El nombre debe tener al menos 3 letras",
        maxlength:"El nombre debe tener al menos 3 letras",

      },
      nombre_cli:{
        required:"Por favor ingrese el primer apellido",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"El apellido debe tener al menos 3 caracteres"
      },
      telf_cli:{
        required:"Ingrese el numero de telefono",
        minlength:"Numero de telefono incorrecto, ingrese 10 digitosa",
        maxlength:"Telefono incorrectoa, ingrese 10 digitosa",

      },
      correo_cli:{
        required:"Por favor ingrese el email",
        minlength:"Direccion incorrecta, ingrese 10 digitosa",
        maxlength:"Dirección incorrecta, ingrese 10 digitosa",


      }
    }
  }
);
</script>
