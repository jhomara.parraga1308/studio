
<section class="banner" id="top">
		<div class="container">
				<div class="row">
						<div class="col-md-10 col-md-offset-1">
								<div class="banner-caption">
										<div class="line-dec"></div>
										<h2>La Belleza Digital</h2>
										<span>Te ofrecemos varios servicion y muchas promociones e incluyen descuentos.</span>
										<div class="blue-button">
												<a class="scrollTo" data-scrollTo="popular" href="#">Descubrir mas </a>
										</div>
								</div>

						</div>
				</div>
		</div>
</section>

<section class="popular-places" id="popular">
		<div class="container-fluid">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Galeria</span>
										<h2>Nuestros Trabajos</h2>
								</div>
						</div>
				</div>
				<div class="owl-carousel owl-theme">
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_1.jpg" alt="">
										<div class="text-content">
												<h4>Montañas</h4>
												<span>76 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_2.jpg" alt="">
										<div class="text-content">
												<h4>Belleza</h4>
												<span>18 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_3.jpg" alt="">
										<div class="text-content">
												<h4>Luces Iluminación</h4>
												<span>55 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_4.jpg" alt="">
										<div class="text-content">
												<h4>Flores</h4>
												<span>66 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_5.jpg" alt="">
										<div class="text-content">
												<h4>Espectro</h4>
												<span>82 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/foto1.jpeg" width="244px" height="249px"alt="">
										<div class="text-content">
												<h4>Animales</h4>
												<span>24 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/foto2.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Ciudades</h4>
												<span>33 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/foto3.jpg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Esculturas</h4>
												<span>62 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/foto4.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Esculturas</h4>
												<span>46 Fotos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_5.jpg" alt="">
										<div class="text-content">
												<h4>Pellentesque</h4>
												<span>85 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_1.jpg" alt="">
										<div class="text-content">
												<h4>Commodo</h4>
												<span>76 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_2.jpg" alt="">
										<div class="text-content">
												<h4>Adipiscing</h4>
												<span>32 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_3.jpg" alt="">
										<div class="text-content">
												<h4>Etiam hendrerit</h4>
												<span>49 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_4.jpg" alt="">
										<div class="text-content">
												<h4>Suspendisse</h4>
												<span>63 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_5.jpg" alt="">
										<div class="text-content">
												<h4>Sit amet dictum</h4>
												<span>86 listings</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section class="featured-places" id="blog">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Lugares destacados</span>
										<h2>Lo mejor de lo mejor</h2>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_1.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>28</h6>
														<span>Junio</span>
												</div>
										</div>
										<div class="down-content">
												<h4>Eventos</h4>
												<span>Categoria Uno</span>
												<p>La organización de eventos es una profesión muy variada y satisfactoria, sobre todo cuando se comprueban los buenos resultados que se tienen tras la celebración de uno de ellos.</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir A Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_2.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>04</h6>
														<span>Julio</span>
												</div>
										</div>
										<div class="down-content">
												<h4>Paisajes</h4>
												<span>Categoria Dos</span>
												<p>La naturaleza, además de ser una fuente de inspiración y relajación, es también fuente de vida.</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir A Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_3.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>15</h6>
														<span>Julio</span>
												</div>
										</div>
										<div class="down-content">
												<h4>Modelaje</h4>
												<span>Categoria Tres</span>
												<p>Una rosa jamás podrá ser un girasol y un girasol jamás podrá ser una rosa. Cada flor es bella a su manera, lo mismo pasa con las mujeres, todas somos hermosas de distintas formas.</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir A Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section class="our-services" id="services">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Otros Servicios</span>
										<h2>El Mejor Sitio de Fotografía</h2>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-4">
								<div class="service-item">
										<div class="icon">
												<img src="plantilla/img/service_icon_1.png" alt="">
										</div>
										<h4>Diseño de alta calidad</h4>
										<p>Diseño y fotografía de excelente calidad, siempre la prioridad son nuestros clientes.</p>
								</div>
						</div>
						<div class="col-md-4">
								<div class="service-item">
										<div class="icon">
												<img src="plantilla/img/service_icon_2.png" alt="">
										</div>
										<h4>Totalmente personalizable</h4>
										<p>Fotografía personalizada al gusto y estilo de nuestros clientes.</p>
								</div>
						</div>
						<div class="col-md-4">
								<div class="service-item">
										<div class="icon">
												<img src="plantilla/img/service_icon_3.png" alt="">
										</div>
										<h4>Mejor producción</h4>
										<p>La mejor producción con el menor riempo posible de entrega para nuestros clientes</p>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-12">
								<div class="down-services">
										<div class="row">
												<div class="col-md-5 col-md-offset-1">
														<div class="left-content">
																<h4>Preguntas Frecuentes</h4>
																<p>Este apartado esta lleno de preguntas más frecuentes de nuestros clientes.<br><br>Con la finalidad de solucionar las dudas e inquietudes de nuestros clientes, siempre ofreciendo calidad.</p>
																<div class="blue-button">
																		<a href="#">Descubre Más</a>
																</div>
														</div>
												</div>
												<div class="col-md-5">
														<div class="accordions">
																<ul class="accordion">
																		<li>
																				<a>¿Cuál es tu estilo de fotografía?</a>
																				<p>Lo primero en lo que nos solemos fijar es en el aspecto o apariencia de algo o alguien. Y con la fotografía sucede lo mismo. Por eso, a la hora de buscar a vuestro fotógrafo ideal, fijaos bien en su trabajo y en el estilo de sus imágenes: tradicional, con poses estudiadas, más fotoperiodísticas</p>
																		</li>
																		<li>
																				<a>¿Qué buscas conseguir con tus fotografías?</a>
																				<p>Aunque en apariencia esta es una de las preguntas para un fotógrafo algo compleja, ¡hacedla y prestad atención a la respuesta! Como profesional, lo ideal sería que el contestase algo profundo, relacionado con las emociones.</p>
																		</li>
																		<li>
																				<a>¿Cómo actuarías ante cualquier posible imprevisto?</a>
																				<p>Imprevistos pueden ser desde que el fotógrafo se ponga enfermo el día de vuestra boda hasta que os llueva en plena ceremonia al aire libre y él/ella se tenga que adaptar al clima. Un fotógrafo especializado en bodas siempre tendrá un plan de acción.</p>
																		</li>
																</ul> <!-- / accordion -->
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section id="video-container">
		<div class="video-overlay"></div>
		<div class="video-content">
				<div class="inner">
						<span>Video de Presentacion</span>
						<h2>Quienes somos?</h2>
						<a href="https://www.youtube.com/watch?v=00vxiXAjNJg" target="_blank"><i class="fa fa-play"></i></a>
				</div>
		</div>
		<video autoplay="" loop="" muted>
			<source src="Qué es la fotografía.mp4" type="video/mp4" />
		</video>
</section>



<section class="pricing-tables">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Tablas de Precios</span>
										<h2>Los mejores precios, ofertas y descuentos</h2>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-4">
								<div class="table-item">
										<div class="top-content">
												<h4>Plan Inicial</h4>
												<h1>$25</h1>
												<span>20 Fotos</span>
										</div>
										<ul>
												<li><a href="#">20 Fotos profesionales</a></li>
												<li><a href="#">Ediciones a tu estilo</a></li>
												<li><a href="#">10 Secciones gratis de fotos</a></li>
												<li><a href="#">Videos personalizados</a></li>
												<li><a href="#">Garantia</a></li>
										</ul>
										<div class="blue-button">
												<a href="#">Compra Ahora</a>
										</div>
								</div>
						</div>
						<div class="col-md-4">
								<div class="table-item">
										<div class="top-content">
												<h4>Plan Premium</h4>
												<h1>$45</h1>
												<span>50 Fotos</span>
										</div>
										<ul>
												<li><a href="#">50 Fotos profesionales</a></li>
												<li><a href="#">Ediciones a tu estilo</a></li>
												<li><a href="#">20 Seccionesn gratis de fotos</a></li>
												<li><a href="#">Videos personalizados</a></li>
												<li><a href="#">Garantia</a></li>
										</ul>
										<div class="blue-button">
												<a href="#">Compra Ahora</a>
										</div>
								</div>
						</div>
						<div class="col-md-4">
								<div class="table-item">
										<div class="top-content">
												<h4>Plan Plus</h4>
												<h1>$85</h1>
												<span>100 Fotos</span>
										</div>
										<ul>
												<li><a href="#">100 Fotos profesionales</a></li>
												<li><a href="#">Ediciones a tu estilo</a></li>
												<li><a href="#">20 Seccionesn gratis de fotos</a></li>
												<li><a href="#">Videos personalizados</a></li>
												<li><a href="#">Garantia</a></li>
										</ul>
										<div class="blue-button">
												<a href="#">Compra Ahora</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section class="contact" id="contact">
		<div id="map">
					<!-- How to change your own map point
											 1. Go to Google Maps
											 2. Click on your location point
											 3. Click "Share" and choose "Embed map" tab
											 4. Copy only URL and paste it within the src="" field below
								-->

				<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=6406" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="container">
				<div class="col-md-10 col-md-offset-1">
						<div class="wrapper">
							<div class="section-heading">
									<span>Contactos</span>
									<h2>Contactanos Para Más Información</h2>
							</div>
							<!-- Modal button -->
							<button id="modBtn" class="modal-btn">Habla con nosotros</button>
						</div>
						<div id="modal" class="modal">
							<!-- Modal Content -->
							<div class="modal-content">
								<div class="close fa fa-close"></div>
								<div class="row">
										<div class="col-md-8">
												<div class="left-content">
														<div class="row">
																<div class="col-md-12">
																		<div class="section-heading">
																				<span>Habla con nosotros</span>
																				<h2>Tengamos una conversación</h2>
																		</div>
																</div>
																<div class="col-md-6">
																	<fieldset>
																		<input name="Nombre" type="text" class="form-control" id="name" placeholder="Su nombre" required="">
																	</fieldset>
																</div>
																 <div class="col-md-6">
																	<fieldset>
																		<input name="Comentario" type="text" class="form-control" id="subject" placeholder="Escribe tu comentario" required="">
																	</fieldset>
																</div>
																<div class="col-md-12">
																	<fieldset>
																		<textarea name="Mensaje" rows="6" class="form-control" id="message" placeholder="Escribe el mensaje" required=""></textarea>
																	</fieldset>
																</div>
																<div class="col-md-12">
																	<fieldset>
																		<button type="submit" id="form-submit" class="btn">Enviar Mensaje</button>
																	</fieldset>
																</div>
														</div>
												</div>
										</div>
										<div class="col-md-4">
												<div class="right-content">
														<div class="row">
																<div class="col-md-12">
																		<div class="content">
																				<div class="section-heading">
																						<span>Más acerca de nosotros</span>
																						<h2>Nuestra Compañia</h2>
																				</div>
																				<p>Comunicate con nosotros para mayor información, para resolver tus dudas e inquietudes, para solicitar una seccion, evento, fotos, etc.</p>
																				<ul>
																						<li><span>Telefono:</span><a href="#">0994447475</a></li>
																						<li><span>Email:</span><a href="#">studiofotografico@gmail.com</a></li>
																						<li><span>Dirección:</span><a href="#">Ecuador - Uio</a></li>
																				</ul>
																		</div>
																</div>
														</div>
												</div>
										</div>
								</div>
							</div>
						</div>
				</div>
		</div>
</section>
