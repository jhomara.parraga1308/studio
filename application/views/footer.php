<body style="background-color:#EBE7F0;">

<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
	toastr.success("<?php echo
	$this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session
     	->set_flashdata("confirmacion","")  ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
	toastr.error("<?php echo
	$this->session->flashdata("error"); ?>");
  </script>
  <?php $this->session
     	->set_flashdata("error","")  ?>
<?php endif; ?>

<?php if ($this->session->flashdata("bienvenida")): ?>
  <script type="text/javascript">toastr.info("<?php echo $this->session->flashdata("bienvenida"); ?>");</script>
  <?php $this->session->set_flashdata("bienvenida",""); ?>
<?php endif; ?>

<style media="screen">
  .obligatorio{
    color:red;
    background-color: white;
    border-radius: 10px;
    font-size: 12px;
    padding-left: 5px;
    padding-right: 5px;
  }

  .error{
    color:red;
    font-weight:bold;
  }
  input.error{
    border:2px solid red;
  }
</style>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="about-veno">
                        <div class="logo">
                            <img src="<?php echo base_url(); ?>plantilla/img/img1.png" alt="Venue Logo">
                        </div>
                        <p>El estudio fotográfico es ante todo un centro de trabajo, por eso debe contar con todos los elementos y herramientas que nos permitan desarrollar plenamente nuestra práctica profesional. Es recomendable que el espacio sea lo suficientemente grande como para que entren cómodamente tanto los equipos como el personal que forma parte del estudio.</p>
                        <ul class="social-icons">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="useful-links">
                        <div class="footer-heading">
                            <h4>Nuestros Links</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="#"><i class="fa fa-stop"></i>Ayuda</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Registrate</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Login</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Mi Perfil</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Como Funciona?</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Más Acerca De Nosotros</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="#"><i class="fa fa-stop"></i>Nuestros Clientes</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Asociaciones</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Blog Entradas</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Contactos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="contact-info">
                        <div class="footer-heading">
                            <h4>Contactos Informacion</h4>
                        </div>
                        <p>Un contacto es la conexión o vínculo que se establece entre dos objetos o sujetos, real o virtual.

Dos objetos o sujetos están en contacto físico cuando son cercanos y en su totalidad o en parte se tocan.</p>
                        <ul>
                            <li><span>Telefono:</span><a href="#">0994447475</a></li>
                            <li><span>Email:</span><a href="#">studiofotografico@gmail.com</a></li>
                            <li><span>Direccion:</span><a href="#">Ecuador - Uio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="sub-footer">
        <p>Copyright &copy; 2018 Company Name

    	- Designado: <a rel="nofollow" href="http://www.templatemo.com">Jonathan Ramos, Christian Quezada, Jhomara Parraga</a> Distribution: <a rel="nofollow" href="https://themewagon.com">Universidad Tecnica De Cotopaxi</a></p>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>plantilla/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="<?php echo base_url(); ?>plantilla/js/vendor/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>plantilla/js/datepicker.js"></script>
    <script src="<?php echo base_url(); ?>plantilla/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>plantilla/js/main.js"></script>
</body>
</html>
