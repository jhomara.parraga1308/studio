<h1 class="text-center"><b>Editar Pedido</b></h1>
<form class=""
id="frm_editar_pedido"
action="<?php echo site_url('pedidos/procesarActualizacion'); ?>" method="post">
    <div class="row">
      <input type="text" name="id_ped" id=id_ped value="<?php echo $pedidoEditar->id_ped; ?>">
      <div class="row">
        <div class="col-md-4">
            <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="text" placeholder="Ingrese el nombre del pedido" class="form-control" name="nombre_ped" value="<?php echo $pedidoEditar->nombre_ped; ?>" id="nombre_ped">
        </div>
        <div class="col-md-4">
            <label for="">Precio:<span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="number" placeholder="Ingrese el precio" class="form-control" name="precio_ped" value="<?php echo $pedidoEditar->precio_ped; ?>" id="precio_ped">
        </div>
        <div class="col-md-4">
          <label for="">Fecha:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="date" min="2023-06-01" max="2023-12-31" placeholder="Ingrese la fecha" class="form-control" name="fecha_ped" value="<?php echo $pedidoEditar->fecha_ped; ?>" id="fecha_ped">
        </div>
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/pedidos/procesarActualizacion"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_nuevo_pedido").validate({
    rules:{
      nombre_ped:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      precio_ped:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
        min: 0,
        max: 1000
      },
      fecha_ped:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:false
      }
    },
    messages:{
      nombre_ped:{
        required:"Por favor ingrese el nombre",
        minlength:"Ingrese un nombre correcto",
        maxlength:"Ingreso incorrecto"
      },
      precio_ped:{
        required:"Por favor ingrese el precio",
        minlength:"El precio debe temer umeros",
        maxlength:"Nombre del evento muy largo",,
        min: "Ingrese un precio mayor a 0",
        max: "Ingrese un precio valido"
      },
      fecha_ped:{
        required:"Por favor ingrese la fecha del pedido",
        minlength:"El nombre dee tener 10 carcateres",
        maxlength:"Fecha muy extensa"
        }
    }
  });
</script>
