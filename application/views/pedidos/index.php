<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Lista de Pedidos</h1>
                    <br>
                    <br>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/pedidos/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Pedido</a>

                  </div>

                  </div>

                  <?php if ($pedidos): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_pedidos">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Precio</th>
                          <th>Fecha</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($pedidos as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_ped ?></td>
                             <td> <?php echo $filaTemporal->nombre_ped ?></td>
                             <td> <?php echo $filaTemporal->precio_ped ?></td>
                             <td> <?php echo $filaTemporal->fecha_ped?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/pedidos/editar/<?php echo $filaTemporal->id_ped; ?>" title="Editar Pedido" style="color:blue;">
                                 <i class="glyphicon glyphicon-pencil">Editar</i>
                               </a>
                               <?php endif; ?>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/pedidos/eliminar/<?php echo $filaTemporal->id_ped; ?>" title="Borrar pedido" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_pedidos").DataTable();
                  </script>

      </div>
  </div>

</div>
