<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Listado de editores</h1>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/editores/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Editor</a>

                  </div>

                  </div>

                  <?php if ($editores): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_editores">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>CEDULA</th>
                          <th>APELLIDO</th>
                          <th>NOMBRE </th>
                          <th>TELEFONO</th>
                          <th>EMAIL</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($editores as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_edi ?></td>
                             <td> <?php echo $filaTemporal->cedula_edi ?></td>
                             <td> <?php echo $filaTemporal->apellido_edi ?></td>
                             <td> <?php echo $filaTemporal->nombre_edi?></td>
                             <td> <?php echo $filaTemporal->telefono_edi?></td>
                             <td> <?php echo $filaTemporal->email_edi ?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/editores/editar/<?php echo $filaTemporal->id_edi; ?>" title="Editar Editor" style="color:red;">
                               <i class="mdi mdi-pencil"></i>
                               Editar
                               </a>
                               <i class="glyphicon glyphicon-pencil"></i>
                               <?php endif; ?>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/editores/eliminar/<?php echo $filaTemporal->id_edi; ?>" title="Borrar editor" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                             <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_editores").DataTable();
                  </script>

      </div>
  </div>

</div>
