<h1 class="text-center"><b>Editar Editor de fotografia</b></h1>
<form class=""
id="frm_editar_editor"
action="<?php echo site_url('editores/procesarActualizacion'); ?>" method="post">
    <div class="row">
      <input type="text" name="id_edi" id=id_edi value="<?php echo $editorEditar->id_edi; ?>">
      <div class="col-md-4">
          <label for="">Cédula:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number" placeholder="Ingrese la cédula" class="form-control" required min="99999999" name="cedula_edi" value="<?php echo $editorEditar->cedula_edi; ?>" id="cedula_edi">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido"
          class="form-control"
          required

          name="apellido_edi" value="<?php echo $editorEditar->apellido_edi; ?>"
          id="apellido_edi">
      </div>
      <div class="col-md-4">
        <label for="">NOMBRE:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre"
        class="form-control"
        name="nombre_edi" value="<?php echo $editorEditar->nombre_edi; ?>"
        id="nombre_edi">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">TELEFONO:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el telefono"
          class="form-control"
          required

          name="telefono_edi" value="<?php echo $editorEditar->telefono_edi; ?>"
          id="telefono_edi">
      </div>
      <div class="col-md-4">
          <label for="">EMAIL:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo"
          class="form-control"
          required

          name="email_edi" value="<?php echo $editorEditar->email_edi; ?>"
          id="email_edi">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Editar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/editores/procesarActualizacion"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<script type="text/javascript">
  $("#frm_editar_editor").validate({
    rules:{
          cedula_edi:{
            required:true,
            minlength:10,
            maxlength:10,
            digits:true
          },
          apellido_edi:{
            required:true,
            minlength:3,
            maxlength:250,
            letras:true
          },
          nombre_edi:{
            required:true,
            minlength:3,
            maxlength:250

          },
          telefono_edi:{
            required:true,
            minlength:3,
            maxlength:250,
            digits:true

          },
          email_edi:{
            required:true,
            minlength:3,
            maxlength:250

          }
    },
    messages:{
      cedula_edi:{
        required:"Cedula icorrecta ingrese 10 digitos",
        minlength:"Cedula incorrecta, ingrese 10 digitosa",
        maxlength:"Cedula incorrecta, ingrese 10 digitosa",
        digits:"Este campo Solo acepta numeros"

      },
      apellido_edi:{
        required:"Por favor ingrese el primer apellido",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"El apellido debe tener al menos 3 caracteres"
      },
      nombre_edi:{
        required:"Este campo solo acepta letras",
        minlength:"El nombre debe tener al menos 3 letras",
        maxlength:"El nombre debe tener al menos 3 letras",

      },
      telefono_edi:{
        required:"Ingrese el numero de telefono",
        minlength:"Numero de telefono incorrecto, ingrese 10 digitosa",
        maxlength:"Telefono incorrectoa, ingrese 10 digitosa",

      },
      email_edi:{
        required:"Por favor ingrese el email",
        minlength:"Direccion incorrecta, ingrese 10 digitosa",
        maxlength:"Dirección incorrecta, ingrese 10 digitosa",


      }
    }
  }
);
</script>
