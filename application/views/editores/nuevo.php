<h1 class="text-center"><b>Nuevo Editor de Fotografia</b></h1>
<form class="" id="frm_nuevo_editor" action="<?php echo site_url(); ?>/editores/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cedula:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="number" placeholder="Ingrese el numero de cedula del editor" class="form-control" name="cedula_edi" value="" id="cedula_edi">
      </div>
      <div class="col-md-4">
          <label for="">Apellido:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el apellido del editor" class="form-control" name="apellido_edi" value="" id="apellido_edi">
      </div>
      <div class="col-md-4">
        <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Ingrese el Nombre del editor" class="form-control" name="nombre_edi" value="" id="nombre_edi">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Telefono:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="number" placeholder="Ingrese el telefono del editor" class="form-control" name="telefono_edi" value="" id="telefono_edi">
      </div>
      <div class="col-md-4">
        <label for="">Email:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="float" placeholder="Ingrese su correo editor" class="form-control" name="email_edi" value="" id="email_edi">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/editores/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
  $("#frm_nuevo_editor").validate({
    rules:{
      cedula_edi:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      apellido_edi:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      nombre_edi:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:false
      },
      telefono_edi:{
        required:true,
        minlength:3,
        maxlength:500,
        letras:true
      },
      email_edi:{
        required:true,
        minlength:3,
        maxlength:300,
        letras:true
      }
    },
    messages:{
      cedula_edi:{
        required:"Por favor ingrese el numero de cedula",
        minlength:"Ingrese un numero de cedula válido",
        maxlength:"Ingreso incorrecto"
      },
      apellido_edi:{
        required:"Por favor ingrese el apellido del cliente",
        minlength:"El nombre del cliente debe tener al menos 5 caracteres",
        maxlength:"Nombre del evento muy largo"
      },
      nombre_edi:{
        required:"Por favor ingrese el nombre del fotografo",
        minlength:"El nombre dee tener 10 carcateres",
        maxlength:"Fecha muy extensa"
      },
      telefono_edi:{
        required:"Por favor ingrese el telefono",
        minlength:"El telefono debe tener 10 umeros",
        maxlength:"TELEFONO incorrecto"
      },
      email_edi:{
        required:"Por favor ingrese el correo",
        minlength:"La dirección debe tener al menos 10 o mas caracteres",
        maxlength:"Dirección de correo Incorrecta"
        }
    }
  });
</script>
