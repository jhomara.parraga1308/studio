<br>
<br>
<div class="main-content">
  <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
                  <br>
                  <div class="row">
                  <div class="col-md-8">
                    <h1 class="text-center">Lista de Promocion</h1>
                    <br>
                    <br>

                  </div>
                  <div class="col-md-4">
                    <a href="<?php echo site_url(); ?>/promociones/nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>Agregar Evento</a>

                  </div>

                  </div>

                  <?php if ($promocion): ?>
                   <table class="table table-striped table-bordered table-hover" id="tbl_promociones">
                     <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Tipo</th>
                          <th>Descuento</th>
                          <th>Codigo</th>
                          <th>Editar</th>
                          <th>Eliminar</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($promocion as $filaTemporal ): ?>
                         <tr>
                             <td class="text-center"> <?php echo $filaTemporal->id_pro ?></td>
                             <td> <?php echo $filaTemporal->nombre_pro ?></td>
                             <td> <?php echo $filaTemporal->tipo_pro ?></td>
                             <td> <?php echo $filaTemporal->descuento_pro?></td>
                             <td> <?php echo $filaTemporal->codigo_pro?></td>
                             <td class="text-center">
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/promociones/editar/<?php echo $filaTemporal->id_pro; ?>" title="Editar Promocion" style="color:blue;">
                                 <i class="glyphicon glyphicon-pencil">Editar</i>
                               </a>
                               <?php endif; ?>
                              &nbsp;&nbsp;
                             </td>
                             <td class="text-center" >
                               <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                               <a href="<?php echo site_url(); ?>/promociones/eliminar/<?php echo $filaTemporal->id_pro; ?>" title="Borrar fotografo" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                               <i class="glyphicon glyphicon-trash">Eliminar</i>
                               </a>
                               <?php endif; ?>
                             </td>

                         </tr>

                       <?php endforeach; ?>
                     </tbody>
                   </table>
                  <?php else: ?>
                  <h1>No hay datos</h1>
                  <?php endif; ?>

                  <script type="text/javascript">
                  $("#tbl_promociones").DataTable();
                  </script>


      </div>
  </div>

</div>
