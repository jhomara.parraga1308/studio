<h1>Editar Promociones</h1>

<form class="" id="frm_editar_promocion" action="<?php echo site_url('promociones/procesarActualizacion'); ?>" method="post">

  <div class="row">

    <div class="col-md-4">
        <label for="">Id:</label>
        <br>
        <input type="number" placeholder="Id" class="form-control" name="id_pro" value="<?php echo $promocionEditar->id_pro ?>">
    </div>



    <div class="col-md-4">
        <label for="">Nombre de Promoción:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Escriba el nombre de la promocion" class="form-control" name="nombre_pro" value="<?php echo $promocionEditar->nombre_pro ?>" id="nombre_pro">
    </div>
    <div class="col-md-4">
        <label for="">Tipo Promoción:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Ingrese el tipo de promocion" class="form-control" name="tipo_pro" value="<?php echo $promocionEditar->tipo_pro ?>" id="tipo_pro">
    </div>
    <div class="col-md-4">
      <label for="">Descuento:</label>
      <br>
      <input type="text" placeholder="Ingrese el porcentaje de la promocion" class="form-control" name="descuento_pro" value="<?php echo $promocionEditar->descuento_pro ?>" id="descuento_pro">
    </div>
    <div class="col-md-4">
      <label for="">Codigo de la Promoción:<span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese el codigo del descuento" class="form-control" name="codigo_pro" value="<?php echo $promocionEditar->codigo_pro ?>" id="codigo_pro">
    </div>
  </div>
  <br>

  <br>
  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Editar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/promociones/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_editar_promociones").validate({
    rules:{
      nombre_pro:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      tipo_pro:{
        required: true,
        minlength:3,
        maxlength:250,
      },
      descuento_pro:{
        required: true,
        minlength:3,
        maxlength:6,
        digist: true,
      },
      codigo_pro:{
        required: true,
        minlength:3,
        maxlength:6,
      },
    },

    messages:{

    nombre_pro:{
      required: "ingrese el nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    tipo_pro:{
      required: "ingrese el tipo",
      minlength:"El tipo debe tener 3 caracteres",
      maxlength:"Tipo incorrecto",
    },
    descuento_pro:{
      required: "ingrese el descuento",
      minlength:"Descuento incorrecto ingrese 3 digitos",
      maxlength:"Descuento incorrecto ingrese 6 digitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
    codigo_pro:{
      required: "ingrese su codigo",
      minlength:"El codigo debe tener 3 caracteres",
      maxlength:"Codigo incorrecto",
    },

  },

});


</script>
