<h1 class="text-center"><b>Nuevas Promociones</b></h1>
<br>
<br>
<form class="" id="frm_nuevo_promociones"action="<?php echo site_url(); ?>/promociones/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre de Promoción:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Escriba el nombre de la promocion" class="form-control" name="nombre_pro" value="" id="nombre_pro">
      </div>
      <div class="col-md-4">
          <label for="">Tipo Promoción:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text" placeholder="Ingrese el tipo de promocion" class="form-control" name="tipo_pro" value="" id="tipo_pro">
      </div>
      <div class="col-md-4">
        <label for="">Promoción:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Ingrese el porcentaje de la promocion" class="form-control" name="descuento_pro" value="" id="descuento_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <label for="">Codigo de la Promoción:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="text" placeholder="Ingrese el codigo del descuento" class="form-control" name="codigo_pro" value="" id="codigo_pro">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/promociones/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">

  $("#frm_nuevo_promociones").validate({
    rules:{
      nombre_pro:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      tipo_pro:{
        required: true,
        minlength:3,
        maxlength:250,
      },
      descuento_pro:{
        required: true,
        minlength:3,
        maxlength:6,
        digist: true,
      },
      codigo_pro:{
        required: true,
        minlength:3,
        maxlength:6,
      },
    },

    messages:{

    nombre_pro:{
      required: "ingrese el nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    tipo_pro:{
      required: "ingrese el tipo",
      minlength:"El tipo debe tener 3 caracteres",
      maxlength:"Tipo incorrecto",
    },
    descuento_pro:{
      required: "ingrese el descuento",
      minlength:"Descuento incorrecto ingrese 3 digitos",
      maxlength:"Descuento incorrecto ingrese 6 digitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
    codigo_pro:{
      required: "ingrese su codigo",
      minlength:"El codigo debe tener 3 caracteres",
      maxlength:"Codigo incorrecto",
    },

  },

});


</script>
